import 'dart:async';

import 'package:champs/pages/home_screen.dart';
import 'package:champs/pages/splash_screen.dart';
import 'package:champs/route_generator.dart';
import 'package:flutter/material.dart';

void main() {
  // ignore: unnecessary_new
  runApp(const MaterialApp(
    debugShowCheckedModeBanner: false,
   initialRoute: '/Splash',
   onGenerateRoute: RouteGenerator.generateRoute,
  ));
}



