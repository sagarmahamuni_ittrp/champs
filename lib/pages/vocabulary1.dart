// ignore_for_file: unnecessary_new, prefer_const_constructors

import 'package:champs/utils/colors.dart';
import 'package:champs/utils/db_images.dart';
import 'package:flutter/material.dart';

import 'drawer.dart';
import 'griddashboard.dart';

// ignore: use_key_in_widget_constructors
class vocabulary1 extends StatefulWidget {
  @override
  _vocabulary1State createState() => new _vocabulary1State();
}

class _vocabulary1State extends State<vocabulary1> {
  double xOffset = 0;
  double yOffset = 0;
  double scaleFactor = 1;
  int _value = 1;

  bool isDrawerOpen = false;
  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      transform: Matrix4.translationValues(xOffset, yOffset, 0)
        ..scale(scaleFactor)
        ..rotateY(isDrawerOpen ? -0.5 : 0),
      duration: const Duration(milliseconds: 5),
      decoration: BoxDecoration(
          color: Colors.grey[200],
          borderRadius: BorderRadius.circular(isDrawerOpen ? 40 : 0.0)),
      child: Scaffold(
        backgroundColor: db_backcolor,
        body: SafeArea(
          child: Stack(
            children: <Widget>[
              Container(
                height: 90.0,
                margin: const EdgeInsets.all(15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      // ignore: prefer_const_literals_to_create_immutables
                      children: <Widget>[
                        IconButton(
                          // ignore: prefer_const_constructors
                          icon: Icon(
                            Icons.menu,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            drawer();
                            print('pressed');
                          },
                        ),

                        // Container(
                        //   decoration: BoxDecoration(
                        //       shape: BoxShape.circle,
                        //       border: Border.all(color: Colors.white, width: 2)),
                        //   child: const CircleAvatar(
                        //     backgroundImage: AssetImage(db_champLogoMain),
                        //     radius: 25,
                        //   ),
                        // ),
                        const SizedBox(
                          width: 16,
                        )
                      ],
                    ),
                    Image.asset(
                      db_champLogo,
                      width: 35.0,
                      fit: BoxFit.fill,
                    ),
                  ],
                ),
              ),
              SingleChildScrollView(
                padding: EdgeInsets.only(top: 100),
                child: Container(
                  height: 750.0,
                  padding: EdgeInsets.only(top: 30, left: 40),
                  alignment: Alignment.topLeft,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(24),
                          topRight: Radius.circular(24))),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        alignment: Alignment.topCenter,
                        margin: EdgeInsets.only(right: 280),
                        child: Text(
                          'Select Type',
                          style: TextStyle(fontSize: 12),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only( right: 230),
                        
                        child: DropdownButton(
                          value: _value,
                          items: const [
                            DropdownMenuItem(
                              child: Text("First Item"),
                              value: 1,
                            ),
                            DropdownMenuItem(
                              child: Text("Second Item"),
                              value: 2,
                            ),
                            DropdownMenuItem(
                              child: Text("Third Item"),
                              value: 3,
                            ),
                            DropdownMenuItem(
                              child: Text("Forth Item"),
                              value: 4,
                            ),
                          ],
                          onChanged: (int? value) {
                            setState(() {
                              _value = value!;
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
